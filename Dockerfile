FROM openjdk:8-jdk-alpine

COPY ./target/assignment-*.jar /app

#ENV SPRING_PROFILE=h2

EXPOSE 8090

ENTRYPOINT  java -jar -Dserver.port=8090 /app

